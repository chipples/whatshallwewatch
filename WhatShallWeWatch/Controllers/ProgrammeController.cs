﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WhatShallWeWatch.DAL;
using WhatShallWeWatch.Models;
using WhatShallWeWatch.Models.TVDB;
using WhatShallWeWatch.ViewModels;

namespace WhatShallWeWatch.Controllers
{
    public class ProgrammeController : Controller
    {
        WhatShallWeWatchDBContext Db { get; set; }
        
        public ProgrammeController()
        {
            Db = new WhatShallWeWatchDBContext();
        }

        //
        // GET: /Programme/
        public ActionResult Index()
        {       
            return View(Db.Programmes);
        }

        //
        // GET: /Programme/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Programme/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Programme/Create
        [HttpPost]
        public ActionResult Create(Programme programme)
        {
            try
            {
                Db.Programmes.Add(programme);
                Db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Programme/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Programme/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Programme/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Programme/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET /Programme/SearchTVDB
        public ActionResult SearchTVDB()
        {
            return View(new SearchTVDBViewModel());
        }

        //
        // POST /Programm/SearchTVDB
        [HttpPost]
        public ActionResult SearchTVDB(SearchTVDBViewModel model)
        {
            TVDBSeriesRepo repo = new TVDBSeriesRepo();
            IEnumerable<TVDBSeries> series = repo.GetSeries(model.SearchTitle);

            return View("SearchResults", series);
        }
    }
}
