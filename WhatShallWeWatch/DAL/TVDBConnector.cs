﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;
using System.Xml.Serialization;
using WhatShallWeWatch.Models.TVDB;

namespace WhatShallWeWatch.DAL
{
    public class TVDBConnector 
    {
        private String urlRoot = "http://thetvdb.com/api/";
        private String getEpisodeCommand = "GetSeries.php?seriesname=";
        private String apiKey = "07B7169285ED0304";
        private XMLDownloader xmlDownloader = new XMLDownloader();

        public XElement LookupEpisodeTitle(string title)
        {
            string searchURL = urlRoot + getEpisodeCommand + title.ToLower().Replace(" ", "+");
            return xmlDownloader.DownloadXML(searchURL);
        }
    }
}