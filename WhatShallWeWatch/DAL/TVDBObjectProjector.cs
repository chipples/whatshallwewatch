﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WhatShallWeWatch.Models.TVDB;

namespace WhatShallWeWatch.DAL
{
    public class TVDBObjectProjector
    {
        public IEnumerable<TVDBSeries> ProjectSeries(XElement seriesXML)
        {
            IEnumerable<TVDBSeries> series = from s in seriesXML.Descendants("Series")
                                             select new TVDBSeries(
                                                 (int)s.Element("seriesid"),
                                                 (string)s.Element("SeriesName"),
                                                 (string)s.Element("Overview"));
            return series;
        }
    }
}