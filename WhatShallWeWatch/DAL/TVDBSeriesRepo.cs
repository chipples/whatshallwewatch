﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WhatShallWeWatch.Models.TVDB;

namespace WhatShallWeWatch.DAL
{
    public class TVDBSeriesRepo
    {
        private TVDBConnector connector = new TVDBConnector();
        private TVDBObjectProjector projector = new TVDBObjectProjector();

        public IEnumerable<TVDBSeries> GetSeries(string title)
        {
            XElement seriesXML = connector.LookupEpisodeTitle(title);
            IEnumerable<TVDBSeries> series = projector.ProjectSeries(seriesXML);

            return series;
        }
    }
}