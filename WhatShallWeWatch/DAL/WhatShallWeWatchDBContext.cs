﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WhatShallWeWatch.Models;

namespace WhatShallWeWatch.DAL
{
    public class WhatShallWeWatchDBContext : DbContext
    {
        public DbSet<Programme> Programmes { get; set; }
        public DbSet<Series> Series { get; set; }
        public DbSet<Episode> Episodes { get; set; }
    }
}