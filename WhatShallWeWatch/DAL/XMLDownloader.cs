﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace WhatShallWeWatch.DAL
{
    public class XMLDownloader
    {
        public XElement DownloadXML(string url)
        {
            string xml;
            
            using (var WebClient = new WebClient())
            {
                xml = WebClient.DownloadString(url);
            }

            return XElement.Parse(xml);
        }
    }
}