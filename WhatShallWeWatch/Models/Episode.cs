﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WhatShallWeWatch.Models
{
    public class Episode
    {
        public int Id { get; set; }
        public int EpisodeNumber { get; set; }
        public TimeSpan RunningTime { get; set; }
        public string Title { get; set; } 
        public int SeriesId { get; set; }
        public Series Series { get; set; }
    }
}