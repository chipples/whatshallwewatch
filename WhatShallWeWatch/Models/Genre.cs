﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WhatShallWeWatch.Models
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Programme> TVShows { get; set; }
    }
}