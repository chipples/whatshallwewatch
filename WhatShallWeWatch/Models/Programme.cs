﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WhatShallWeWatch.Models;

namespace WhatShallWeWatch.Models
{
    public class Programme
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<Genre> Genres { get; set; }
        public ICollection<Series> Series { get; set; }
    }
}