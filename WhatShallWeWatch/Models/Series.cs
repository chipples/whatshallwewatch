﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WhatShallWeWatch.Models
{
    public class Series
    {
        public int Id { get; set; }
        public int SeriesNumber { get; set; }
        public TimeSpan NormalEpisodeLength { get; set; }
        public int ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        
        public ICollection<Episode> Episodes { get; set; }
    }
}