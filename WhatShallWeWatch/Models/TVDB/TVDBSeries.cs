﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WhatShallWeWatch.Models.TVDB
{
    public class TVDBSeries
    {
        public int SeriesId { get; set; }
        public string Title { get; set; }
        public string Overview { get; set; }

        public TVDBSeries(int id, string title, string overview)
        {
            SeriesId = id;
            Title = title;
            Overview = overview;
        }

        public TVDBSeries() { }
    }
}