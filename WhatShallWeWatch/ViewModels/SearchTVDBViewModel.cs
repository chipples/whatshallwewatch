﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WhatShallWeWatch.ViewModels
{
    public class SearchTVDBViewModel
    {
        public int Id { get; set; }
        public string SearchTitle { get; set; }
    }
}